# Examples

Some examples:

    ansible-playbook -i inventory-vm.ini -u <remote_user> -K playbook-vm-debian.yaml

    ansible -i inventory-vm.ini virtualmachines -u <remote_user> --ask-pass -m ping

'virtualmachines' is the "pattern".